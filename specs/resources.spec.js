const request = require("supertest")
const app = require("../src/server")

describe("GET /resources", () => {
  it("responds as json", async () => {
    const response = await request(app).get("/resources")

    expect(response.status).toEqual(200)
    expect(response.type).toEqual("application/json")
  })

  it("returns a list of resources", async () => {
    const response = await request(app).get("/resources")

    aCommonResource = {
      name: expect.any(String),
      categories: expect.any(Array),
      url: expect.any(String),
    }
    expect(response.body).toBeInstanceOf(Array)
    expect(response.body).toContainEqual(aCommonResource)
  })

  it("accepts resources with date and location", async () => {
    const response = await request(app).get("/resources")

    aResourceWithDateAndLocation = {
      name: expect.any(String),
      categories: expect.any(Array),
      url: expect.any(String),
      date: expect.any(String),
      location: expect.any(String),
    }
    expect(response.body).toBeInstanceOf(Array)
    expect(response.body).toContainEqual(aResourceWithDateAndLocation)
  })
})
