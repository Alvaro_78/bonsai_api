const request = require("supertest")
const app = require("../src/server")

describe("GET /categories", () => {
  it("responds as json", async () => {
    const response = await request(app).get("/categories")

    expect(response.status).toEqual(200)
    expect(response.type).toEqual("application/json")
  })

  it("returns a list of categories", async () => {
    const response = await request(app).get("/categories")

    anyCategory = "Escuelas"
    expect(response.body).toBeInstanceOf(Array)
    expect(response.body).toContainEqual(anyCategory)
  })
})
