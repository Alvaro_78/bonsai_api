const Categories = require("../../src/collections/Categories")

describe("Categories", () => {
  it("fetchs resources", async () => {
    const resources = await Categories.fetchAll()

    expect(resources).toContainEqual("Escuelas")
  })

  it("orders resources alphabetically", async () => {
    const resources = await Categories.fetchAll()

    expect(resources[0]).toEqual("Artículos")
  })
})
