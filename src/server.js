const express = require("express")
const cors = require("cors")
const server = express()

server.use(cors())

server.get("/", (_, response) => response.send("OK"))

server.get("/resources", async (_, response) => {
  const collection = require("./collections/Resources")
  const resources = await collection.fetchAll()

  response.send(resources)
})

server.get("/categories", async (_, response) => {
  const collection = require("./collections/Categories")
  const categories = await collection.fetchAll()

  response.send(categories)
})

module.exports = server
