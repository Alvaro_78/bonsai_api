const resources = require("../../data/resources")

module.exports = {
  fetchAll: async () => {
    return resources.sort((a, b) => {
      const nameA = a.name.toLocaleLowerCase()
      const nameB = b.name.toLocaleLowerCase()

      if (nameA < nameB) {
        return -1
      }
      if (nameA > nameB) {
        return 1
      }

      return 0
    })
  },
}
