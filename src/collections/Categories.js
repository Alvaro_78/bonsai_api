const resources = require("../../data/resources")

module.exports = {
  fetchAll: async () => {
    const allCategories = resources.flatMap((resource) => resource.categories)
    const uniqueCategories = [...new Set(allCategories).values()]
    const sortedCategories = uniqueCategories.sort()

    return sortedCategories
  },
}
