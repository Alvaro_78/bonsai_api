module.exports = [
  {
    name: "Escuela Bonsai Valencia",
    categories: ["Escuelas"],
    url: "http://escuelabonsaivalencia.es",
  },
  {
    name: "Centro Bonsai Alboraya",
    categories: ["Tiendas"],
    url: "https://www.centrobonsai.com",
  },
  {
    name: "El TIM Bonsai",
    categories: ["Escuelas"],
    url: "https://www.facebook.com/eltim.bonsai",
  },
  {
    name: "Asociación del Bonsai Español",
    categories: ["Asociaciones"],
    url: "https://www.facebook.com/ABEBonsai",
  },
  {
    name: "Unión del Bonsai Español",
    categories: ["Asociaciones"],
    url: "https://ubebonsai.es",
  },
  {
    name: "El Bonsái y yo - Loli Avilés",
    categories: ["Artículos"],
    url: "https://www.lolibonsai.com",
  },
  {
    name: "Bonsai Nostrum",
    categories: ["Tiendas"],
    url: "https://www.bonsainostrum.com",
  },
  {
    name: "Bonsai Granada",
    categories: ["Escuelas", "Tiendas"],
    url: "https://www.bonsaigranada.com",
  },
  {
    name: "Bonsái Pavía",
    categories: ["Escuelas"],
    url: "https://www.bonsaipavia.es",
  },
  {
    name: "Medibonsai - Bonsai del Mediterraneo",
    categories: ["Tiendas"],
    url: "http://medibonsai.com",
  },
  {
    name: "Gabriel Romero - Bonsaisantboi",
    categories: ["Tiendas"],
    url: "https://www.facebook.com/gabrielromerobonsaisantboi",
  },
  {
    name: "Studio Bonsai David Quintana",
    categories: ["Escuelas", "Tiendas"],
    url: "https://dqbonsai.com",
  },
  {
    name: "Estudio Bonsai David Benavente",
    categories: ["Escuelas", "Tiendas"],
    url: "https://davidbenavente.com",
  },
  {
    name: "Bonsai Sense",
    categories: ["Tiendas"],
    url: "https://www.bonsaisense.com",
  },
  {
    name: "Bonsai Empire",
    categories: ["Tiendas"],
    url: "https://www.bonsaiempire.es",
  },
  {
    name: "Escuela de Bonsai Kaeru-En 蛙園",
    categories: ["Escuelas"],
    url: "https://www.escueladebonsaionline.com",
  },
  {
    name: "Kingii | Bonsáis",
    categories: ["Tiendas", "Escuelas"],
    url: "https://www.kingii.es",
  },
  {
    name: "Bonsais La Perla",
    categories: ["Tiendas"],
    url: "https://bonsaislaperla.net",
  },
  {
    name: "Bonsai Colmenar",
    categories: ["Tiendas"],
    url: "https://www.bonsaicolmenar.com",
  },
  {
    name: "Escuela Sagunt Bonsái",
    categories: ["Escuelas"],
    url: "https://www.saguntbonsai.es",
  },
  {
    name: "Mistral Bonsai",
    categories: ["Tiendas"],
    url: "https://www.mistralbonsai.com",
  },
  {
    name: "Lombrico Bonsai",
    categories: ["Tiendas"],
    url: "http://lombricobonsai.com",
  },
  {
    name: "Bonsaikido",
    categories: ["Tiendas"],
    url: "https://bonsaikido.com",
  },
  {
    name: "Bonsai Levante",
    categories: ["Escuelas"],
    url: "https://www.bonsailevante.com",
  },
  {
    name: "Naturdecora",
    categories: ["Tiendas"],
    url: "https://naturdecora.com",
  },
  {
    name: "Mercadillo Mercabonsai",
    categories: ["Tiendas"],
    url: "https://www.facebook.com/groups/1417529361826180",
  },
  {
    name: "El rastrillo del bonsai",
    categories: ["Tiendas"],
    url: "https://www.facebook.com/groups/1738600986238832",
  },
  {
    name: "El Mercadillo del Bonsai",
    categories: ["Tiendas"],
    url: "https://www.facebook.com/groups/1738600986238832",
  },
  {
    name: "El Kumi Bonsai",
    categories: ["Escuelas"],
    url: "https://www.facebook.com/el.kumi.7",
  },
  {
    name: "Bonsái Jaume Canals",
    categories: ["Tiendas", "Escuelas"],
    url: "https://www.jaumecanals.com/es",
  },
  {
    name: "Jose Antonio Guerao",
    categories: ["Cerámicas"],
    url: "https://www.facebook.com/joseantonio.gueraonavarro",
  },
  {
    name: "Laos Garden",
    categories: ["Tiendas"],
    url: "https://www.laosgarden.com/es",
  },
  {
    name: "Crataegus Bonsai Cofrentes",
    categories: ["Asociaciones"],
    url: "https://www.facebook.com/groups/bonsaicofrentes2",
  },
  {
    name: "II Congreso ABE",
    categories: ["Exposiciones"],
    date: "5 y 6 de diciembre 2020",
    location: "Fuenlabrada, Madrid",
    url: "http://www.abebonsai.es",
  },
  {
    name: "III Convención UBEbonsai",
    categories: ["Exposiciones"],
    date: "29, 30 y 31 de enero 2021",
    location: "Aranjuez, Madrid",
    url: "https://ubebonsai.es/index.php/iii-ubebonsai",
  },
  {
    name: "Alberto Gimeno, pinos thumbergii desde semilla",
    categories: ["Vídeos"],
    url: "https://youtu.be/FCRshqKQCNE",
  },
  {
    name: "Luis Vila nos enseña como hace los pinos silvestris desde plantón",
    categories: ["Vídeos"],
    url: "https://youtu.be/zkDGo77JrYo",
  },
]
